#!/bin/bash
#==============================================================================
# Copyright (c) 2015 BOI. All rights reserved.
#
# Program:          launchtivoli.sh
# Author:           
#==============================================================================
# TEST

# Define what the program is about.
#
ABOUT="
Program : launchtivoli.sh
Author  : 

Return codes:
                0 - Normal exit.
                1 - An error has occured.
                2 - Information has been displayed, perhaps due to incorrect usage.
"


#!/bin/bash
echo "----------------------Start of launchtivoli.sh -------------------------------------\n\n"


CLASSPATH=/home/was8/tivoli/HelloWorld/build/jar/HelloWorld.jar
java -cp $CLASSPATH HelloWorld


echo "----------------------End of launchtivoli.sh -------------------------------------\n\n"
exit 0



