#!/bin/bash
#==============================================================================
# Copyright (c) 2015 BOI. All rights reserved.
#
# Program:          tivoliserver.sh
# Author:           
#==============================================================================
# TEST

# Define what the program is about.
#
ABOUT="
Program : tivoliserver.sh
Author  : 

Return codes:
                0 - Normal exit.
                1 - An error has occured.
                2 - Information has been displayed, perhaps due to incorrect usage.
"


#!/bin/bash
echo "----------------------Start of tivoliserver.sh -------------------------------------\n\n"
alertmsg=$1



var="tivoli_write_Msg"

   myfile="/home/was8/tivoli/shellscript/File_Source_Alert_Messages/"$var".txt"

    if [[ -e $myfile ]]; 
 then 

/bin/echo "$var file already exists"


     else

        myfile="/home/was8/tivoli/shellscript/File_Source_Alert_Messages/"tivoli_write_Msg"".txt"
	/usr/bin/touch $myfile
	 echo "$alertmsg" >> ${myfile}

	fi	



echo "----------------------End of tivoliserver.sh-------------------------------------\n\n"
exit 0


