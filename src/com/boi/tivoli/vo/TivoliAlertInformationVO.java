/*
 * TivoliAlertInformationVO.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.vo;

import java.io.Serializable;

/**
 * This class will consist all setter/getter methods of various values fetched
 * from tables used in tivoli application.
 */
public class TivoliAlertInformationVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String SITUATION = null;
	private String SITUATION_ID_PK = null;
	private String RESOLVER_GROUP = null;
	private String APPLICATION = null;
	private String CONFIGURATION_ITEM = null;
	private String SEVERITY = null;
	private String CATEGORY = null;
	private String IT_SERVICE_IMPACTED = null;
	private String BUSINESS_IMPACTED = null;
	private String ERROR_CATALOG_LINK = null;
	private int ITERATIONS_LIMIT = 0;
	private int ITERATIONS_PERIOD = 0;
	private String onCallNumber = null;
	private String emailGroupName = null;
	private String nullalertvo = null;
	private String alertMessage = null;
	private String OLD_INC_REFERENCE = null;

	public String getOLD_INC_REFERENCE() {
		return OLD_INC_REFERENCE;
	}

	public void setOLD_INC_REFERENCE(String oLD_INC_REFERENCE) {
		OLD_INC_REFERENCE = oLD_INC_REFERENCE;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

	public String getEmailGroupName() {
		return emailGroupName;
	}

	public void setEmailGroupName(String emailGroupName) {
		this.emailGroupName = emailGroupName;
	}

	private boolean chksitrwcunt = false;

	public void setITERATIONS_PERIOD(int nITERATIONS_PERIOD) {
		ITERATIONS_PERIOD = nITERATIONS_PERIOD;
	}

	public int getITERATIONS_PERIOD() {
		return ITERATIONS_PERIOD;
	}


	public void setonCallNumber(String nonCallNumber) {
		onCallNumber = nonCallNumber;
	}

	public String getonCallNumber() {
		return onCallNumber;
	}

	public void setSITUATION(String nSITUATION) {
		SITUATION = nSITUATION;
	}

	public String getSITUATION() {
		return SITUATION;
	}

	public void setSEVERITY(String nSEVERITY) {
		SEVERITY = nSEVERITY;
	}

	public String getSEVERITY() {
		return SEVERITY;
	}

	public void setSITUATION_ID_PK(String nSITUATION_ID_PK) {
		SITUATION_ID_PK = nSITUATION_ID_PK;
	}

	public String getSITUATION_ID_PK() {
		return SITUATION_ID_PK;
	}


	public void setAPPLICATION(String nAPPLICATION) {
		APPLICATION = nAPPLICATION;
	}

	public String getAPPLICATION() {
		return APPLICATION;
	}


	public void setCATEGORY(String nCATEGORY) {
		CATEGORY = nCATEGORY;
	}

	public String getCATEGORY() {
		return CATEGORY;
	}

	public void setERROR_CATALOG_LINK(String nERROR_CATALOG_LINK) {
		ERROR_CATALOG_LINK = nERROR_CATALOG_LINK;
	}

	public String getERROR_CATALOG_LINK() {
		return ERROR_CATALOG_LINK;
	}

	public void setITERATIONS_LIMIT(int nITERATIONS_LIMIT) {
		ITERATIONS_LIMIT = nITERATIONS_LIMIT;
	}

	public int getITERATIONS_LIMIT() {
		return ITERATIONS_LIMIT;
	}

	public void setIT_SERVICE_IMPACTED(String nIT_SERVICE_IMPATED) {
		IT_SERVICE_IMPACTED = nIT_SERVICE_IMPATED;
	}

	public String getIT_SERVICE_IMPACTED() {
		return IT_SERVICE_IMPACTED;
	}

	public void setBUSINESS_IMPACTED(String nBUSINESS_IMPACTED) {
		BUSINESS_IMPACTED = nBUSINESS_IMPACTED;
	}

	public String getBUSINESS_IMPACTED() {
		return BUSINESS_IMPACTED;
	}


	public void setAlertNullVO(String nalert) {
		nullalertvo = nalert;
	}

	public String getAlertNullVO() {
		return nullalertvo;
	}

	public void setCheckSituationRowCount(boolean nCSRC) {
		chksitrwcunt = nCSRC;
	}

	public boolean getCheckSituationRowCount() {
		return chksitrwcunt;
	}
	public String getRESOLVER_GROUP() {
		return RESOLVER_GROUP;
	}

	public void setRESOLVER_GROUP(String rESOLVER_GROUP) {
		RESOLVER_GROUP = rESOLVER_GROUP;
	}

	public String getCONFIGURATION_ITEM() {
		return CONFIGURATION_ITEM;
	}

	public void setCONFIGURATION_ITEM(String cONFIGURATION_ITEM) {
		CONFIGURATION_ITEM = cONFIGURATION_ITEM;
	}
}