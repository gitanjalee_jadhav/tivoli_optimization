/*
 * TivoliAlertAdapter.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */

package com.boi.tivoli.business;

import java.io.StringWriter;

import org.apache.log4j.Logger;

import com.boi.tivoli.common.constants.TivoliErrorConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;
import com.boi.tivoli.vo.TivoliAlertInformationVO;

/**
 * This class will be invoked by TivoliAlertController and delegates the call to
 * the business bean class for further processing.
 */

public class TivoliAlertAdapter {

	private TivoliAlertBussinessBean tAlertBussinessBean = null;
	private boolean ptavAdapter = false;
	private boolean emailsend = false;
	private boolean piadapter = false;
	private boolean pAlertStoragebkp = false;
	private StringWriter emailadapter = null;;

	private static Logger cLogger = Logger.getLogger(TivoliAlertAdapter.class);
	public Boolean cIsDebug = false;
	
	public TivoliAlertAdapter() {
		tAlertBussinessBean = new TivoliAlertBussinessBean();
		cIsDebug = cLogger.isDebugEnabled();
	}

	/**
	 * This method delegates the Alert Identify Activity for collecting alert
	 * information and send to business bean class for further process.
	 */

	public TivoliAlertInformationVO processTivoliAlertIdentifyAdapter(
			String pTivoliAlert, TivoliAlertInformationVO taInformationVO)
			throws TivoliAlertException {
		try {
			taInformationVO = tAlertBussinessBean.handleAlertIdentifyEvent(pTivoliAlert,
					taInformationVO);
		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return taInformationVO;
	}
	


	/**
	 * This method delegates the Alert Validation Activity to the business bean
	 * class for further process
	 */

	public boolean processTivoliAlertValidationAdapter(
			TivoliAlertInformationVO taInformationVO)
			throws TivoliAlertException {

		try {
			ptavAdapter = tAlertBussinessBean.handleAlertValidationEvent(
					taInformationVO);

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return ptavAdapter;
	}

	/**
	 * This method delegates the Alert email transform/template creation Activity to the business
	 * bean class for further process.
	 */
	public StringWriter processTivoliAlertTransformAdapter( 
			TivoliAlertInformationVO taInformationVO)
			throws TivoliAlertException {

		try {
			emailadapter = tAlertBussinessBean
					.handleTivoliEmailTemplateEvent(taInformationVO);

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return emailadapter;
	}

	/**
	 * This method delegates the Alert email Sender Activity to the business
	 * bean class for further process.
	 */
	public boolean processEmailSenderAdapter(StringWriter pEmailStr ,TivoliAlertInformationVO tainformVO)
			throws TivoliAlertException {

		try {
			emailsend = tAlertBussinessBean.handleEmailSenderEvent(pEmailStr ,tainformVO );

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return emailsend;
	}

	/**
	 * This method delegates the Alert information to store into AlertStorage.xls file
	 * Activity to the business bean class for further process.
	 */
	public boolean processInsertAdapter(TivoliAlertInformationVO taInformationVO)
			throws TivoliAlertException {

		try {
			piadapter = tAlertBussinessBean.handleInsertEvent(taInformationVO);

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return piadapter;
	}
	
	/**
	 * This method create new blank AlertStorage.xls file with headers only
	 * This will be used in purging of AlertStorage.xls file requirement.
	 */
	public boolean processAlertStorageFileCreation()
			throws TivoliAlertException {

		try {
			pAlertStoragebkp = tAlertBussinessBean.handleStrorageFileCreateEvent();

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTADAPTER,
					e.toString());
		}
		return pAlertStoragebkp;
	}
}