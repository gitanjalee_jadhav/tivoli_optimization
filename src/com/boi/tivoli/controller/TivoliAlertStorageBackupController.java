/*
 * TivoliAlertStorageBackupController.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 *June 	12, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.boi.tivoli.business.TivoliAlertAdapter;
import com.boi.tivoli.common.constants.TivoliAlertConstants;
import com.boi.tivoli.common.constants.TivoliErrorConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;
import com.boi.tivoli.common.exception.TivoliExceptionHandler;
import com.boi.tivoli.common.util.TivoliAlertProperties;

/**
 * This class will be responsible for renaming AlertStorage.xls file and creating new blank Alertstorage.xls file with headers only.
 * This is used in purging activity of AlertStorage.xls file.
 */
public class TivoliAlertStorageBackupController {

	private TivoliAlertAdapter tAlertStorageAdapter = null;
	private static TivoliAlertStorageBackupController tiBackupController = null;
	private static TivoliAlertProperties prop = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertStorageBackupController.class);
	public static Boolean cIsDebug = false;
	private static TivoliExceptionHandler teHandler = null;

	public TivoliAlertStorageBackupController() {
		cIsDebug = cLogger.isDebugEnabled();
	}
	/**
	 * This main method is entry point for Tivoli Optimization Alert storage file purge activity
	 * First AlertStirage.xls file is renamed ex. AlertStorage_Backup_JUN_2018.xls and then new blank AlertStorage.xls is created with headers only.
	 */
	public static void main(String[] args) {
		try {
			
			prop = TivoliAlertProperties.getInstance();
			PropertyConfigurator.configure(prop
					.getProperty(TivoliAlertConstants.logj));
			//Rename AlertStorage.xls file to AlertStorage_Backup_month_year.xls ex. AlertStorage_Backup_JUN_2018.xls
			
			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy"); 
	        String currentDate = formatter.format(cal.getTime());
	        //Obtain backup file name  using AlertStorage.xls
	        String strStorageFileName = (String)prop.getProperty(TivoliAlertConstants.alertStorageExcel);
	        String strStorageBKPFolder = (String)prop.getProperty(TivoliAlertConstants.alertStorageExcelBackupFolder);
	        String strStorageBKPFilename = (String)prop.getProperty(TivoliAlertConstants.alertStorageExcelBackupFileName);
	        tiBackupController = new TivoliAlertStorageBackupController();
	        if (cIsDebug){
				cLogger.debug("TivoliAlertStorageBackupController main() method Start ::: ");
			}
	        	String newAlertStorageFileName = strStorageBKPFolder+strStorageBKPFilename+"_"+currentDate+".xls";
	        	File originalFile  = new File(strStorageFileName);
			 	File backupFile  = new File(newAlertStorageFileName);
			 	if (cIsDebug){
					cLogger.debug("In TivoliAlertStorageBackupController ->backup filename ::: "+backupFile);
				}
			 	//Create backup by calling copyFile()
			 	boolean copyFileSuccess = tiBackupController.copyFile(originalFile, backupFile);
			 	if(copyFileSuccess){
		            //Create new blank/empty AlertStorage.xls file
			 		if (cIsDebug){
						cLogger.debug("::: TivoliAlertStorageBackupController backup file created/overwritten successfully!!  ::: ");
					}
			 		tiBackupController.createNewAlertStorageXLSFile();
			 		
		        }else{
		        	if (cIsDebug){
						cLogger.debug("::: TivoliAlertStorageBackupController main() method copyFileSuccess false ::: ");
					}
		        	throw new TivoliAlertException(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER,
							TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEFILECOPY);
		        }
			if (cIsDebug){
				cLogger.debug("TivoliAlertStorageBackupController main() method end ::: ");
			}
		} catch (TivoliAlertException ex) {
			cLogger.error(ex.getMessage());
			teHandler = new TivoliExceptionHandler();
			try {
				teHandler.handleException(ex);
			} catch (TivoliAlertException e1) {
				cLogger.error(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER
								+ ex.toString());
				cLogger.fatal(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER
								+ ex.toString());
			}
		}catch (Exception e) {
				if (e.toString() != null) {
					cLogger.error(e.getMessage());
					cLogger.error(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER
							+ e.toString());
			} 
		}
	}
	
	/**
	 * This method will copy source file data to dest file
	 * This activity is used in purging of AlertStorage file. 
	 * If dest file is already present, it will be overwritten.
	 */
	private  boolean copyFile(File source, File dest) throws TivoliAlertException {
		
	    try {
	    	if (cIsDebug){
				cLogger.debug("TivoliAlertStorageBackupController copyFileUsingApacheCommonsIO() method Start ::: ");
			}
			FileUtils.copyFile(source, dest);
			
		} catch (IOException e) {
			cLogger.error("Exception in TivoliAlertStorageBackupController->copyFileUsingApacheCommonsIO()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER,
					e.toString());
		}
	    if (cIsDebug){
			cLogger.debug("TivoliAlertStorageBackupController copyFileUsingApacheCommonsIO() method End ::: ");
		}
	    return true;
	}
	
	/**
	 * This method will create new blank AlertStorage.xls file with headers only.
	 * This activity is used in purging of AlertStorage file. 
	 */
	public void createNewAlertStorageXLSFile()
			throws TivoliAlertException {
		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertStorageBackupController createNewAlertStorageXLSFile() method Start ::: ");
			}
			tAlertStorageAdapter = new TivoliAlertAdapter();
			tAlertStorageAdapter.processAlertStorageFileCreation();
			
		} catch (TivoliAlertException e) {
			cLogger.error("TivoliAlertException in TivoliAlertStorageBackupController->createNewAlertStorageXLSFile()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER,
					e.toString());

		} catch (Exception e) {
			cLogger.error("Exception in TivoliAlertStorageBackupController->createNewAlertStorageXLSFile()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTSTORAGEBACKUPCONTROLLER,
					e.toString());
		}
		if (cIsDebug){
			cLogger.debug("TivoliAlertStorageBackupController createNewAlertStorageXLSFile() method end ::: ");
		}
	}
}