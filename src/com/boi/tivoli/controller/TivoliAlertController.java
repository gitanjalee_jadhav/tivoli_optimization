/*
 * TivoliAlertController.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Timer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.boi.tivoli.business.TivoliAlertAdapter;
import com.boi.tivoli.common.constants.TivoliAlertConstants;
import com.boi.tivoli.common.constants.TivoliErrorConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;
import com.boi.tivoli.common.exception.TivoliExceptionHandler;
import com.boi.tivoli.common.util.TivoliAlertLogWriter;
import com.boi.tivoli.common.util.TivoliAlertProperties;
import com.boi.tivoli.vo.TivoliAlertInformationVO;

/**
 * This class will be responsible for processing alert txt file and fetch related configurations and send the email as per 
 * details.
 */
public class TivoliAlertController {

	private TivoliAlertAdapter tAlertAdapter = null;
	private StringWriter lEmailTemplateSw = null;
	private StringWriter emailwriter = null;
	private TivoliAlertInformationVO tainformVO;
	private boolean isValidAlert = false;
	private static byte[] bytes = null;
	private static TivoliAlertController tivControl = null;
	private static String tivoliAlert = null;
	private static WatchService watchService = null;
	private static boolean valid = false;
	private static WatchKey watchKey = null;
	private static String filestring = null;
	private static Path path = null;
	private static Path newtempDir = null;
	private static TivoliAlertProperties prop = null;
	private static TivoliExceptionHandler teHandler = null;
	private static String fromLocation = null;
	private static String toLocation = null;
	private static String moveFile = null;
	private static File oldFile = null;
	private static File newFile = null;
	private static InputStream in = null;
	private static OutputStream out = null;
	private static byte[] moveBuff = null;
	private static int butesRead = 0;
	private static Logger cLogger = Logger.getLogger(TivoliAlertController.class);
	public static Boolean cIsDebug = false;
	public static Boolean isException = false;
	public static Timer utilityStatuslogWriter = null;
	

	public TivoliAlertController() {
		cIsDebug = cLogger.isDebugEnabled();
	}
	/**
	 * This main method is entry point to Tivoli optimization java utility for alert text file processing and email generation.
	 */
	public static void main(String[] args) {

		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertController main() method Start ::: ");
			}
			prop = TivoliAlertProperties.getInstance();
			PropertyConfigurator.configure(prop
					.getProperty(TivoliAlertConstants.logj));

			newtempDir = Paths.get(prop
					.getProperty(TivoliAlertConstants.monitor));
			watchService = FileSystems.getDefault().newWatchService();
			newtempDir.register(watchService,
					StandardWatchEventKinds.ENTRY_CREATE);
			valid = true;
			//Start one more thread to write log in tivoli utility log file utility running status
			utilityStatuslogWriter = new Timer(); // Instantiate Timer Object
			TivoliAlertLogWriter st = new TivoliAlertLogWriter(); // Instantiate SheduledTask class
			utilityStatuslogWriter.schedule(st, 0, Long.parseLong(prop
					.getProperty(TivoliAlertConstants.tivoliUtilityLogTime)) * 60 * 1000); // Create Repetitively task for every 10 secs

			do {
				watchKey = watchService.take();
				for (WatchEvent event : watchKey.pollEvents()) {
					WatchEvent.Kind kind = event.kind();
					@SuppressWarnings("unchecked")
					WatchEvent<Path> ev = (WatchEvent<Path>) event;
					if (StandardWatchEventKinds.OVERFLOW.equals(event.kind())) {
						continue;
					}
					if (StandardWatchEventKinds.ENTRY_CREATE.equals(event
							.kind())) {
						try {

							filestring = "" + ev.context();
							prop = TivoliAlertProperties.getInstance();
							path = FileSystems
									.getDefault()
									.getPath(
											prop.getProperty(TivoliAlertConstants.monitor),
											filestring);
							if (cIsDebug){
								cLogger.debug("Alert file path::: "+prop.getProperty(TivoliAlertConstants.monitor)+"/"+filestring);
							}
							tivControl = new TivoliAlertController();
							bytes = Files.readAllBytes(path);
							tivoliAlert = new String(bytes);
							
							if (tivoliAlert != null) {
								if (cIsDebug){
									cLogger.debug("Alert received length::: "+tivoliAlert.length());
									cLogger.debug("Alert received::: "+tivoliAlert);
								}
								tivControl.processAlertMonitoring(tivoliAlert
										.trim());
							} else {
								throw new TivoliAlertException(
										TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
										TivoliErrorConstants.TIVOLI_MSG_ALERT_NULL);
							}

						} catch (TivoliAlertException ex) {
							moveFile(
									filestring,
									prop.getProperty(TivoliAlertConstants.failloc));
							teHandler = new TivoliExceptionHandler();
							teHandler.handleException(ex);
						}
					}
				}
				valid = watchKey.reset();

			} while (valid);

			if (cIsDebug){
				cLogger.debug("TivoliAlertController main() method end ::: ");
			}
		} catch (Exception e) {
			utilityStatuslogWriter.cancel();
			try {
				if (e.toString() != null) {
					cLogger.error(e.getMessage());
					moveFile(filestring,
							prop.getProperty(TivoliAlertConstants.failloc));
					throw new TivoliAlertException(
							TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
							e.toString());
				}
			} catch (TivoliAlertException ex) {
				cLogger.error(e.getMessage());
				utilityStatuslogWriter.cancel();
				cLogger.error(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER
								+ ex.toString());
				cLogger.fatal(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER
								+ ex.toString());

			}

		}
	}
	/**
	 * This method will be move processed alert txt file to success/fail file location.
	 */
	public static void moveFile(String fileName, String whichPath)
			throws TivoliAlertException {

		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertController moveFile() method Start ::: ");
			}
			prop = TivoliAlertProperties.getInstance();
			fromLocation = prop.getProperty(TivoliAlertConstants.monitor);
			toLocation = whichPath;
			moveFile = fileName;
			toLocation = toLocation + moveFile;
			oldFile = new File(fromLocation+fileName);
			newFile = new File(toLocation);
			in = new FileInputStream(oldFile);
			out = new FileOutputStream(newFile);
			moveBuff = fileName.getBytes();
			while ((butesRead = in.read(moveBuff)) > 0) {
				out.write(moveBuff, 0, butesRead);
			}
			in.close();
			out.close();
			oldFile.delete();
			if (cIsDebug){
				cLogger.debug("TivoliAlertController moveFile() method File :::"+fileName+ "  moved to location ::"+whichPath);
			}
		} catch (IOException e) {
			cLogger.error(e);
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
					TivoliErrorConstants.TIVOLI_MSG_MOVEFILE);
		} catch (Exception e) {
			cLogger.error(e);
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
					e.toString());
		}
	}
	
	/**
	 * This method will be process alert string and generate mail and its related activities ex. write alert details to xls file.
	 */
	public void processAlertMonitoring(String alertstr)
			throws TivoliAlertException {
		boolean isEmailSent =false;
		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertController processAlertMonitoring() method Start ::: ");
			}
			if (alertstr != null) {
				tainformVO = new TivoliAlertInformationVO();
				if (tainformVO == null) {
					throw new TivoliAlertException(
							TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
							TivoliErrorConstants.TIVOLI_MSG_VO_NULL);
				}

				tAlertAdapter = new TivoliAlertAdapter();

				tainformVO = tAlertAdapter.processTivoliAlertIdentifyAdapter(
						alertstr, tainformVO);


				if (tainformVO.getCheckSituationRowCount()) {

					isValidAlert = tAlertAdapter.processTivoliAlertValidationAdapter(
							tainformVO);

					if (tainformVO != null) {

						if (isValidAlert) {
							
							if (cIsDebug){
								cLogger.debug("In  TivoliAlertController :: In checkSituationRowCount =true and  isValidAlert=true");
							}

							lEmailTemplateSw = tAlertAdapter
									.processTivoliAlertTransformAdapter(tainformVO);

							isEmailSent = tAlertAdapter
							.processEmailSenderAdapter(lEmailTemplateSw , tainformVO);

							tAlertAdapter.processInsertAdapter(tainformVO);
							moveFile(
									filestring,
									prop.getProperty(TivoliAlertConstants.successloc));

						} else {

							if (cIsDebug){
								cLogger.debug("In  TivoliAlertController :: In checkSituationRowCount =true and  isValidAlert=false");
							}
							tAlertAdapter.processInsertAdapter(tainformVO);
							moveFile(
									filestring,
									prop.getProperty(TivoliAlertConstants.successloc));
						}
					}
				} else {

					//set below property to send mail with alert text and write it as as in Alert storage file
					if (cIsDebug){
						cLogger.debug("In  TivoliAlertController :: In checkSituationRowCount=false");
					}
					tainformVO.setAlertNullVO(alertstr);
					isEmailSent = processDefaultAlert(tainformVO);
					moveFile(
							filestring,
							prop.getProperty(TivoliAlertConstants.successloc));
				}

			} else {
				cLogger.error("Alert message is NULL OR empty");
				throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
						TivoliErrorConstants.TIVOLI_MSG_ALERT_NULL);

			}
		} catch (TivoliAlertException e) {
			cLogger.error("TivoliAlertException in TivoliAlertController->processAlertMonitoring()::"+e.toString());
			//Even if any exception in utility,alert mail should be sent, so added below code and also checked if email is sent or not if not then only send mail 
			//otherwise duplicate mail situation occur.
			if(!isEmailSent)
			{
				tainformVO.setAlertNullVO(alertstr);
				processDefaultAlert(tainformVO);
			}
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
					e.toString());

		} catch (Exception e) {
			if(!isEmailSent)
			{
				tainformVO.setAlertNullVO(alertstr);
				processDefaultAlert(tainformVO);
			}
			cLogger.error("Exception in TivoliAlertController->processAlertMonitoring()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
					e.toString());
		}
	}

	/**
	 * This method will send default alert in case of any exception / alert not found in config file and  write alert details to AlertStorage.xls file.
	 */
	public boolean  processDefaultAlert(TivoliAlertInformationVO tInformationVO)
			throws TivoliAlertException {
		boolean emailSent;
		try
		{
			if (cIsDebug){
				cLogger.debug("TivoliAlertController processDefaultAlert() method Start ::: ");
			}
			emailwriter = new StringWriter();
			emailSent  = tAlertAdapter.processEmailSenderAdapter(emailwriter
					.append(tainformVO.getAlertNullVO()) , tainformVO);
			tAlertAdapter.processInsertAdapter(tainformVO);
		}catch (Exception e) {
			cLogger.error("Exception in TivoliAlertController->processDefaultAlert()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTCONTROLLER,
					e.toString());
		}
		if (cIsDebug){
			cLogger.debug("TivoliAlertController processDefaultAlert() method end ::: ");
		}
		
		return emailSent;
	}
}