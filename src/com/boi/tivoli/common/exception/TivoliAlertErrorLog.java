/*
 * TivoliAlertWriteLog.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.exception;

import org.apache.log4j.Logger;

import com.boi.tivoli.common.constants.TivoliErrorConstants;

/**
 * This class is responsible for logging the exception into the Error Log file,
 * with appropriate error details.
 */
public class TivoliAlertErrorLog {
	private static Logger cLogger = Logger.getLogger(TivoliAlertErrorLog.class);
	public Boolean cIsDebug = false;

	public TivoliAlertErrorLog() {
		cIsDebug = cLogger.isDebugEnabled();
	}
	
	/**
	 * This method takes instance of TivoliAlertException class as input. Using
	 * the error code, fetches error details from TivoliErrorConstants and logs
	 * the log Files.
	 */

	public void writeToErrorLog(TivoliAlertException tException)
			throws TivoliAlertException {

		try {
			String er_code1 = tException
					.getErrorInfo(TivoliErrorConstants.TIVOLI_MSG_ERROR_CODE);
			String er_code2 = tException
					.getErrorInfo(TivoliErrorConstants.TIVOLI_MSG_STACKTRACE);
			cLogger.debug(er_code1 + " " + er_code2);
			cLogger.fatal(er_code1 + " " + er_code2);
		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTWRITELOG,
					e.toString());
		}
	}
}
