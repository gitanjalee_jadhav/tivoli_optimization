/*
 * TivoliExceptionHandler.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.exception;

import com.boi.tivoli.common.constants.TivoliErrorConstants;

/**
 * This class is responsible for handling the exception and performing logging
 * activity.
 */
public class TivoliExceptionHandler {

	private TivoliAlertErrorLog tawLog = null;

	/**
	 * This method logs the Error in error log file
	 */

	public void handleException(TivoliAlertException tException)
			throws TivoliAlertException {

		try {
			tawLog = new TivoliAlertErrorLog();
			tawLog.writeToErrorLog(tException);
		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIEXCEPTIONHANDLER,
					e.toString());

		}

	}

}
