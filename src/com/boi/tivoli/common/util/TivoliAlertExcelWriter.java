/*
 * TivoliAlertExcelWriter.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.boi.tivoli.common.constants.TivoliAlertConstants;
import com.boi.tivoli.common.constants.TivoliErrorConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;

/**
 * This class is responsible for writing alert details to excel AlertStorage.xls file after processing.
 */
public class TivoliAlertExcelWriter {
	
	private TivoliAlertProperties prop = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertExcelWriter.class);
	public Boolean cIsDebug = false;

	public TivoliAlertExcelWriter() {
		cIsDebug = cLogger.isDebugEnabled();
	}

	/**
	 * This method is responsible for writing alert details to excel AlertStorage.xls file after processing.
	 */
	public void writeToExcel(String pAlertMessage,String pResolverGrp,String pSevierty, String pApplication, String pSituationID, String configItem,String pCurrentTime)
			throws TivoliAlertException {
		
		if (cIsDebug){
			cLogger.debug("TivoliAlertExcelWriter writeToExcel() method Start ::: ");
		}
		HSSFWorkbook lWorkBook = null;
		try{
			prop = TivoliAlertProperties.getInstance();
			pAlertMessage = pAlertMessage.replaceAll("\\s", " "); 
        FileInputStream lTivoliFileInputStream = new FileInputStream(prop.getProperty(TivoliAlertConstants.alertStorageExcel));
        
        POIFSFileSystem lTivolifsFileSystem = new POIFSFileSystem(lTivoliFileInputStream);

        lWorkBook = new HSSFWorkbook(lTivolifsFileSystem);
        HSSFSheet lHssfSheet = lWorkBook.getSheetAt(0);
        int currentrows = lHssfSheet.getPhysicalNumberOfRows();
		
	    HSSFRow row = lHssfSheet.createRow(currentrows);
        
	    HSSFCell cell = row.createCell(0);
    	cell.setCellValue(pAlertMessage);

	    HSSFCell cell1 = row.createCell(1);
	    cell1.setCellValue(pSituationID);
	    
	    HSSFCell cell2 = row.createCell(2);
	    cell2.setCellValue(pResolverGrp);

	    HSSFCell cell3 = row.createCell(3);
	    cell3.setCellValue(pSevierty);

	    HSSFCell cell4 = row.createCell(4);
	    cell4.setCellValue(pApplication);

	    HSSFCell cell5 = row.createCell(5);
	    cell5.setCellValue(configItem);
	    
	    HSSFCell cell6 = row.createCell(6);
	    cell6.setCellValue(pCurrentTime);

    	
	    lTivoliFileInputStream.close();
    	
    	FileOutputStream lTivoliOutputFile = new FileOutputStream(prop.getProperty(TivoliAlertConstants.alertStorageExcel));
    	lWorkBook.write(lTivoliOutputFile);
        //workBook.close();
    	lTivoliOutputFile.close();
        if (cIsDebug){
			cLogger.debug("Alert successfully stored in AlertStorage xls file with alert text:: "+pAlertMessage);
		}
	    }catch(Exception e){
	       cLogger.error("Exception in  TivoliAlertExcelWriter method  writeToExcel () :::" +e.toString());
	    	throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEXCELWRITER,
					e.toString());
	    }
	}
	
	/**
	 * This method is responsible for creating new AlertStorage.xls with headers for purging requirement..
	 */
	private void createAlertStorageXLS(String alertStorageFileName)throws TivoliAlertException{
		try
		{
		HSSFWorkbook new_workbook = new HSSFWorkbook(); //create a blank workbook object
		HSSFSheet sheet = new_workbook.createSheet();  //create a worksheet with caption score_details
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); //create a map and define data
		excel_data.put("1", new Object[] {"Alert", "Situation_ID", "ISM Resolver Group","Severity", "Application", "Configuration Item","Alert Creation Time"}); //add data
		Set<String> keyset = excel_data.keySet();
		int rownum = 0;
		for (String key : keyset) { //loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object [] objArr = excel_data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellValue((String)obj);
			}
		}

		FileOutputStream output_file = new FileOutputStream(new File(alertStorageFileName)); //create XLS file
		new_workbook.write(output_file);//write excel document to output stream
		output_file.close(); //close the file
		}catch(IOException ex)
		{
			cLogger.error(ex.getMessage());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
					ex.toString());
		}catch(Exception e){
        	cLogger.error("Exception in  TivoliAlertExcelUtil.checkAlertTime():: "+e);
	        throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
					e.toString());
        }
	}
	public static void main(String are[]){
		TivoliAlertExcelWriter obj = new TivoliAlertExcelWriter();
	//	obj.writeToExcel("Alert");
	}
}
