/*
 * TivoliAlertExcelUtil.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoli.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.boi.tivoli.common.constants.TivoliAlertConstants;
import com.boi.tivoli.common.constants.TivoliErrorConstants;
import com.boi.tivoli.common.exception.TivoliAlertException;
import com.boi.tivoli.vo.TivoliAlertInformationVO;

/**
 * This class is responsible for writing alert details to excel AlertStorage.xls file after processing.
 */
public class TivoliAlertExcelUtil {

	private static TivoliAlertExcelUtil tivoliAlertExcelUtil=null;
	private TivoliAlertProperties prop = null;
	private static int timeDifference;
	private static int alertThreshold ;
	
	double bytes = 0;
	double kilobytes = 0;
	double megabytes = 0;
	public static Boolean cIsDebug = false;
	private static Logger cLogger = Logger.getLogger(TivoliAlertExcelUtil.class);
	
	private TivoliAlertExcelUtil()
	{
		
	}
	public static TivoliAlertExcelUtil getInstance(){
        if(tivoliAlertExcelUtil == null){
        	tivoliAlertExcelUtil = new TivoliAlertExcelUtil();
        }
    	cIsDebug = cLogger.isDebugEnabled();
        return tivoliAlertExcelUtil;
    }
	/**
	 * This method will validate alert date time with property /config file settings related to alert threshold and time difference for comparison.
	 */
	public boolean checkAlertValidation(TivoliAlertInformationVO pAlertInformationVO)
			throws TivoliAlertException {
		boolean isValid=false;
		String lAlertSituation = null;
		 try {
			 	if (cIsDebug){
					cLogger.debug("TivoliAlertExcelUtil checkAlertValidation() method Start ::: ");
				}
			 	prop = TivoliAlertProperties.getInstance();
			 	timeDifference = pAlertInformationVO.getITERATIONS_PERIOD();
			 	alertThreshold = pAlertInformationVO.getITERATIONS_LIMIT();
			 	lAlertSituation = pAlertInformationVO.getSITUATION_ID_PK();
			 	
	            FileInputStream lAlertFileInputStream = new FileInputStream(new File(prop.getProperty(TivoliAlertConstants.alertStorageExcel)));
	            
	            // Create Workbook instance holding reference to .xlsx file
	            HSSFWorkbook lWorkbook = new HSSFWorkbook(lAlertFileInputStream);
	            
	            // Get first/desired sheet from the workbook
	            HSSFSheet lHssfSheet = lWorkbook.getSheetAt(0);

	            if(null != lAlertSituation && lAlertSituation.length() > 0 )
	            {
	            	int lalertCount = checkAlertIteration(lAlertSituation, lHssfSheet);
	            	if (cIsDebug){
	            		cLogger.debug("Alert count in TivoliAlertExcelUtil.checkAlertValidation()::: "+lalertCount);
	            	}
	            	
	            	if(lalertCount == 0 || (lalertCount > 0 && lalertCount < alertThreshold))
	            	{
	            		isValid=true;
	            	}
	            }
	            if (cIsDebug){
	            	cLogger.debug("isValid in TivoliAlertExcelUtil.checkAlertValidation()::: "+isValid);
	            }
	            lAlertFileInputStream.close();
	            if (cIsDebug){
					cLogger.debug("TivoliAlertExcelUtil checkAlertValidation() method end ::: ");
				}
	        } catch (IOException e) {
	        	cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
					e.toString());
		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
					e.toString());
		}
		return isValid;
	}

	/**
	 * This method will count alert's occurrences in AlertStorage.xls file to compare it with alert's config file setting column ->Threshold
	 */	
	 private static int checkAlertIteration(String pAlertSituation, HSSFSheet pSheet) throws TivoliAlertException{
		 if (cIsDebug){
				cLogger.debug("TivoliAlertController checkAlertIteration() method start ::: ");
			}
	        // This parameter is for appending sheet rows to mergedSheet in the end
	        int alertCount=0;
	        //Iterate rows
	        try
	        {
		        for (int excelRow = pSheet.getFirstRowNum(); excelRow <= pSheet.getLastRowNum(); excelRow++) {
	
		            HSSFRow row = pSheet.getRow(excelRow);
		            //Iterate columns
		            for (int excelCol = row.getFirstCellNum(); excelCol < row.getLastCellNum(); excelCol++) {
		                HSSFCell cell = row.getCell(excelCol+1);
		                
		                
		               //Search value based on cell type
		               if(HSSFCell.CELL_TYPE_STRING == cell.getCellType()) {
		            	   
		            	   if(null!= cell.getStringCellValue() && cell.getStringCellValue().length() > 0 
		            			   && pAlertSituation.equalsIgnoreCase(cell.getStringCellValue())) {
		                      //get last column value as it is creation time
		                        HSSFCell cellTime = row.getCell(6); //This is 4th position is row's(Alert creation date) cell ->
		                        if(cellTime.getCellType() == HSSFCell.CELL_TYPE_STRING ) {
		                            String msgTime = cellTime.getStringCellValue();
			                            if(null != msgTime && timeDifference(msgTime))
			                            {
			                            	if (cIsDebug){
		                            		cLogger.debug("TivoliAlertController checkAlertIteration() ->alert found on row number ::: " + excelRow);
		                            	}
			                        	alertCount++ ;
			                        }
		                        }
		                    }
		                    break;
		                }
		            }
		        }
	        }catch (Exception e) {
				cLogger.error(e.toString());
				throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
						e.toString());
			}
	        if (cIsDebug){
				cLogger.debug("TivoliAlertExcelUtil checkAlertIteration() method end ::: ");
			}
	        return alertCount;
	    }
	    
	 	/**
		 * This method will check time difference between current date and alert's creation date
		 */
	    private static boolean timeDifference (String date1)  throws TivoliAlertException
	    {
	    	 boolean isLessMins = false;
	    	 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
	    	 try
	    	 {
	    		 Date startDate = formatter.parse(date1);
	    		 Calendar previous = Calendar.getInstance();
	    		 previous.setTime(startDate);
	    		 Calendar now = Calendar.getInstance();
	    		 long diff = now.getTimeInMillis() - previous.getTimeInMillis();
	    		 if( !(diff > timeDifference * 60 * 1000))
	    		 {
	    			 //difference is less than iteration_period/timedifference
	    			 isLessMins=true;
	    		 }
	    	 }
	    	 catch (ParseException e)
	    	 {
	    		 cLogger.error(e.toString());
	    		 throw new TivoliAlertException(
	    				 TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
	    				 e.toString());

	    	 }catch (Exception e) {
	    		 cLogger.error(e.toString());
	    		 throw new TivoliAlertException(
	    				 TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
	    				 e.toString());
	    	 }
	        return isLessMins;     
	    }

		/**
		 * This method will search alert in alert config file to fetch alert's configurations from config file tivoli.xls
		 */
	    public TivoliAlertInformationVO searchAlert(String pAlertText) throws TivoliAlertException{
			
			if (cIsDebug){
				cLogger.debug("TivoliAlertExcelUtil->searchAlert Method Start ::: ");
			}		
	        List lCellDataList = new ArrayList();
	        HSSFWorkbook lWorkBook = null;
	        FileInputStream lTivoliFileInputStream =null;
	        int totalColumns = 0;
	        try {
	        	
	        	prop = TivoliAlertProperties.getInstance();
	        	lTivoliFileInputStream = new FileInputStream(prop.getProperty(TivoliAlertConstants.alertconfigfile));
	            
	            POIFSFileSystem lTivolifsFileSystem = new POIFSFileSystem(lTivoliFileInputStream);
	            lWorkBook = new HSSFWorkbook(lTivolifsFileSystem);
	            HSSFSheet lHssfSheet = lWorkBook.getSheetAt(0);
	            Iterator rowIterator = lHssfSheet.rowIterator();
	            while (rowIterator.hasNext()) {
	                HSSFRow lHssfRow = (HSSFRow) rowIterator.next();
	                //Below code is added to avoid IndexOutofBoundException if last cell of any alert is blank.Here setting columnCount for each row = no. of  columns of header row
	                if(totalColumns == 0)
	                {
	                	totalColumns = lHssfRow.getPhysicalNumberOfCells();
	                }
	                List lCellTempList = new ArrayList();
	                for (int excelCol = lHssfRow.getFirstCellNum(); excelCol < totalColumns; excelCol++) {
	                	HSSFCell hssfCell = lHssfRow.getCell(excelCol);
	                	//Below if block is for handling blank alert config
	                	if (hssfCell == null || hssfCell.getCellType() == hssfCell.CELL_TYPE_BLANK) {
	                		hssfCell = lHssfRow.createCell(excelCol);
	                		hssfCell.setCellValue("");
	                	}
	                	lCellTempList.add(hssfCell);
	                }
	                lCellDataList.add(lCellTempList);
	            }
	        } catch (Exception e) {
	        	cLogger.error("Exception in TivoliAlertExcelUtil->searchAlert() :: "+e);
	        	throw new TivoliAlertException(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
						e.toString());
	        }

	        TivoliAlertInformationVO lAlertInformationVO =  new TivoliAlertInformationVO();
	        lAlertInformationVO.setAlertMessage(pAlertText);
	        try{
	        	for (int i = 0; i < lCellDataList.size(); i++) {
	        		List cellTempList = (List) lCellDataList.get(i);

	        		for (int j = 0; j < cellTempList.size(); j++) {
	        			HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
	        		
	        			String stringCellValue = hssfCell.toString();
	        			if(j==0)
	        			{
	        				//Checked empty stringCellValue as blank value case also passed here for contains check
	        				if(null != stringCellValue && stringCellValue.length() > 0 
	        						&& pAlertText.replaceAll("\\s+","").contains(stringCellValue.replaceAll("\\s+","")) ) 
	        				{	
	        					if (cIsDebug){
	        						cLogger.debug("Found the Alert ::: "+stringCellValue);
	        					}		
	        					
	        					lAlertInformationVO.setSITUATION(stringCellValue);
	        					j++;
	        					
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setSITUATION_ID_PK(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert situation ID Set in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					j++;
	        					
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setRESOLVER_GROUP(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert Servicenow resolvergroup Set in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setAPPLICATION(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("alert App set Set in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}

	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setSEVERITY(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert sev Set in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}

	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setCONFIGURATION_ITEM(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got config item  in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					
	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setCATEGORY(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert category in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					
	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setIT_SERVICE_IMPACTED(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert service impacted in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					
	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setBUSINESS_IMPACTED(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert business impacted in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					
	        					j++;
	        					//Checking below condition as if error_catalog_link is blank in xls sheet that column will not be fetched so added below to avoid IndexOutOfBoundException 
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setERROR_CATALOG_LINK(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert error_catlog in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					
	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.getStringCellValue();
	        					lAlertInformationVO.setOLD_INC_REFERENCE(stringCellValue);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert old_inc_ref  in  TivoliAlertExcelUtil searchAlert()::"+stringCellValue);
	        					}
	        					
	        					j++;
	        					int iteration_limit =0;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					if (hssfCell!= null && hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
	        						iteration_limit = (int) hssfCell.getNumericCellValue();
	    	                	}
	        					lAlertInformationVO.setITERATIONS_LIMIT(iteration_limit);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert iteration_limit in  TivoliAlertExcelUtil searchAlert()::"+iteration_limit);
	        					}
	        					
	        					j++;
	        					int iterations_period = 0;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					if (hssfCell!= null && hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
	        						iterations_period = (int) hssfCell.getNumericCellValue();
	    	                	}
	        					lAlertInformationVO.setITERATIONS_PERIOD(iterations_period);
	        					if (cIsDebug){
	        						cLogger.debug("Got alert iterations_period in  TivoliAlertExcelUtil searchAlert()::"+iterations_period);
	        					}
	        					
	        					j++;
	        					hssfCell = (HSSFCell) cellTempList.get(j);
	        					stringCellValue = hssfCell.toString();
	        					lAlertInformationVO.setEmailGroupName(stringCellValue);

	        					String onCallNumber = fetchOncallNumber(lAlertInformationVO.getAPPLICATION(),lWorkBook);
	        					if(cIsDebug)
	        					{
	        						cLogger.debug("Got alert onCall number in  TivoliAlertExcelUtil searchAlert()::"+onCallNumber);
	        					}
	        					lAlertInformationVO.setonCallNumber(onCallNumber);
	        					//Set checksituationCount =true as record found in excel sheet
	        					lAlertInformationVO.setCheckSituationRowCount(true);
	        					
	        					String alertTimings =  checkTimeBasedSev(pAlertText,lWorkBook);
	        					if(cIsDebug)
	        					{
	        						cLogger.debug("Alert found in Time Based in  TivoliAlertExcelUtil searchAlert()::"+alertTimings);
	        					}

	        					if(alertTimings != null){

	        						StringTokenizer st = new StringTokenizer(alertTimings,"#");
	        						while (st.hasMoreElements()) {
	        							String startTime = (String)st.nextElement();
	        							String endTime = (String)st.nextElement();
	        							String newSev = (String)st.nextElement();
	        							if(checkDateTimeValidation(startTime) && checkDateTimeValidation(endTime))
	        							{
	        								if(checkAlertTime(startTime,endTime)){
	        									lAlertInformationVO.setSEVERITY(newSev);
	        									if(cIsDebug)
	        		        					{
	        										cLogger.debug("New Sev SET in  TivoliAlertExcelUtil searchAlert():::"+newSev);
	        		        					}
	        									
	        								}
	        							}else
	        							{
	        								if(cIsDebug)
        		        					{
        										cLogger.debug("StartTime OR EndTime is invalid in time based sheet");
        		        					}
	        							}
	        						}
	        					}
	        					break;
	        				}
	        			}
	        		}
	        	}
	        	lTivoliFileInputStream.close();
	        }catch(Exception e){
	        	cLogger.error("Exception in TivoliAlertExcelUtil :: "+e);
	        	throw new TivoliAlertException(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,e.getMessage());
	        }
	        if (cIsDebug){
	        	cLogger.debug("TivoliAlertExcelUtil->searchAlert Method End :::");
	        }		

	        return lAlertInformationVO;
		}
	    
	    /**
		 * This method will fetch alert's onCall number from Tivoli.xls  config file ->sheet no. 2
		 */
	    private String fetchOncallNumber(String application,HSSFWorkbook workBook) throws TivoliAlertException {
	    	
	    	if (cIsDebug){
	        	cLogger.debug("TivoliAlertExcelUtil->fetchOncallNumber Method start :::");
	        }	
	        HSSFSheet hssfSheetOnCall = workBook.getSheetAt(1);
	        Iterator rowIteratorOnCall = hssfSheetOnCall.rowIterator();
	        List cellDataList = new ArrayList();
	        String onCallNumber = null;
	        try{
	        while (rowIteratorOnCall.hasNext()) {
	            HSSFRow hssfRow = (HSSFRow) rowIteratorOnCall.next();
	            Iterator iterator = hssfRow.cellIterator();
	            List cellTempList = new ArrayList();
	            while (iterator.hasNext()) {
	                HSSFCell hssfCell = (HSSFCell) iterator.next();
	                cellTempList.add(hssfCell);
	            }
	            cellDataList.add(cellTempList);
	        }
	        for (int i = 0; i < cellDataList.size(); i++) {
	            List cellTempList = (List) cellDataList.get(i);
	            for (int j = 0; j < cellTempList.size(); j++) {
	                HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
	                String stringCellValue = hssfCell.toString();
	                if(j==0){
	                    
	                if(application.equalsIgnoreCase(stringCellValue)) {	
	            				
	                	j++;
	                    hssfCell = (HSSFCell) cellTempList.get(j);
	                    onCallNumber = hssfCell.toString();
	                    if (cIsDebug){
	            			cLogger.debug("Found the Oncall Number ::: "+stringCellValue);
	            		}
	                }
	                }
	            }
	        }
	        } catch (Exception e) {
	        	cLogger.error("Exception in TivoliAlertExcelUtil->fetchOncallNumber() :: "+e);
	        	throw new TivoliAlertException(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,e.getMessage());
	        }
	        if (cIsDebug){
	        	cLogger.debug("TivoliAlertExcelUtil->fetchOncallNumber Method end :::");
	        }
	        return onCallNumber;
	    }
	    
	    /**
		 * This method will check time based severity for alert in alert config file tivoli.xls->sheet no.3
		 */
	    private String checkTimeBasedSev(String alert,HSSFWorkbook workBook) throws TivoliAlertException{
	    	if (cIsDebug){
	        	cLogger.debug("TivoliAlertExcelUtil->checkTimeBasedSev Method start :::");
	        }
	        
			HSSFSheet hssfSheetTimeBase = workBook.getSheetAt(2);
	        Iterator rowIteratorTimeBase = hssfSheetTimeBase.rowIterator();
	        List cellDataList = new ArrayList();
	        String alertTiming = null;
			
	        try{
	        while (rowIteratorTimeBase.hasNext()) {
	            HSSFRow hssfRow = (HSSFRow) rowIteratorTimeBase.next();
	            Iterator iterator = hssfRow.cellIterator();
	            List cellTempList = new ArrayList();
	            while (iterator.hasNext()) {
	                HSSFCell hssfCell = (HSSFCell) iterator.next();
	                cellTempList.add(hssfCell);
	            }
	            cellDataList.add(cellTempList);
	        }
	        for (int i = 0; i < cellDataList.size(); i++) {
	        	List cellTempList = (List) cellDataList.get(i);
	        	for (int j = 0; j < cellTempList.size(); j++) {
	        		HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
	        		String stringCellValue = hssfCell.toString();
	        		if(j==0){
	        			if(alert.replaceAll("\\s+","").contains(stringCellValue.replaceAll("\\s+",""))) {
	        				if (cIsDebug){
	        					cLogger.debug("Found the alert in Timebased sheet in method checkTimeBasedSev() ::: "+stringCellValue);
	        				}		
	        				j++;
	        				hssfCell = (HSSFCell) cellTempList.get(j);
	        				String alertStartTime = hssfCell.toString();

	        				j++;
	        				hssfCell = (HSSFCell) cellTempList.get(j);
	        				String alertEndTime = hssfCell.toString();

	        				j++;
	        				hssfCell = (HSSFCell) cellTempList.get(j);
	        				String newSev = hssfCell.toString();

	        				alertTiming = alertStartTime + "#" + alertEndTime + "#" + newSev;
	        			}
	        		}
	        	}
	        }
	    } catch (Exception e) {
	    	cLogger.error("Exception in fetchOncallNumber :: "+e);
	        throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
					e.toString());
	    }
	        if (cIsDebug){
	        	cLogger.debug("TivoliAlertExcelUtil->checkTimeBasedSev Method end :::");
	        }
			return alertTiming;
		}

		/**
		 * This method will check if alert's time falls in particular timespan provided in alert config file  tivoli.xls ->sheet no. 3
		 */
		public static boolean checkAlertTime(String startTime,String endTime) throws TivoliAlertException{
	        if (cIsDebug) {
				cLogger.debug("TivoliAlertExcelUtil-> checkAlertTime Starting ..");
			}
			boolean alertontime = false;
	        try{
	       	Date endtime = new Date();
	       	Date starttime = new Date();
	       	
	        Calendar cal = Calendar.getInstance();
	    	Date d = cal.getTime();
	    	List<Integer> time = new ArrayList<Integer>(); 
	            
	        StringTokenizer st = new StringTokenizer(startTime,":");
	        while (st.hasMoreElements()) {
	        	time.add(Integer.parseInt((String)st.nextElement()));
			}
	        if(time.size()==3){
		    	cal.set(Calendar.HOUR_OF_DAY,time.get(0));
		    	cal.set(Calendar.MINUTE,time.get(1));
		    	cal.set(Calendar.SECOND,time.get(2));
		    	starttime = cal.getTime();
		    	time.clear();
	        }

	        StringTokenizer st1 = new StringTokenizer(endTime,":");
	        while (st1.hasMoreElements()) {
	        	time.add(Integer.parseInt((String)st1.nextElement()));
			}
	        
	        if(time.size()==3){
		    	cal.set(Calendar.HOUR_OF_DAY,time.get(0));
		    	cal.set(Calendar.MINUTE,time.get(1));
		    	cal.set(Calendar.SECOND,time.get(2));
		    	endtime = cal.getTime();
	        }
	        if (cIsDebug) {
		        cLogger.debug("Inside checkAlertTime() d.after(starttime):: "+d.after(starttime));
		        cLogger.debug("Inside checkAlertTime() d.before(endtime):: "+d.before(endtime));
	        }
	    	if(d.after(starttime) && d.before(endtime)){
	    		alertontime = true;
	    	}else {
	    		alertontime = false;
	    	}
	        
	        }catch(Exception e){
	        	cLogger.error("Exception in  TivoliAlertExcelUtil.checkAlertTime():: "+e);
		        throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
						e.toString());
	        }
	        if (cIsDebug) {
				cLogger.debug("TivoliAlertExcelUtil-> checkAlertTime End ..");
			}

			return alertontime;
		}
		
		/**
		 * This method will check if time given in alert config file  tivoli.xls ->sheet no. 3 in format HH:MM:SS for time based severity ex. 00:01:11
		 */
		private boolean checkDateTimeValidation(String strDateTime)
		{
			String specialCharacters=":0123456789";
		    String str2[]=strDateTime.split("");
	
		    for (int i=0;i<str2.length;i++)
		    {
		    	if (!specialCharacters.contains(str2[i]))
		    	{
		    		if (cIsDebug) {
						cLogger.debug("TivoliAlertExcelUtil-> checkDateTimeValidation :-> strDateTime Contains nonnumeric & other than ':' value,so invalid strDateTime.");
					}
		    		return false;
		    	}
		    }
		    return true;
		}
		
		/**
		 * This method will create new AlertStorage.xls file with headers only. This is used in purge for AlertStorage.xls.
		 */
		public boolean createAlertStorageXLS()throws TivoliAlertException{
			try
			{
				if (cIsDebug) {
					cLogger.debug("TivoliAlertExcelUtil-> createAlertStorageXLS() Start ..");
				}
				prop = TivoliAlertProperties.getInstance();
				HSSFWorkbook new_workbook = new HSSFWorkbook(); //create a blank workbook object
				HSSFSheet sheet = new_workbook.createSheet();  //create a worksheet with caption score_details
				Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); //create a map and define data
				excel_data.put("1", new Object[] {"Alert", "Situation_ID", "ISM Resolver Group","Severity", "Application", "Configuration Item","Alert Creation Time"}); //add data
				Set<String> keyset = excel_data.keySet();
				int rownum = 0;
				for (String key : keyset) { //loop through the data and add them to the cell
					Row row = sheet.createRow(rownum++);
					Object [] objArr = excel_data.get(key);
					int cellnum = 0;
					for (Object obj : objArr) {
						Cell cell = row.createCell(cellnum++);
						cell.setCellValue((String)obj);
					}
				}
				FileOutputStream output_file = new FileOutputStream(new File(prop.getProperty(TivoliAlertConstants.alertStorageExcel))); //create XLS file
				new_workbook.write(output_file);//write excel document to output stream
				output_file.close(); //close the file
				if (cIsDebug) {
					cLogger.debug("TivoliAlertExcelUtil-> createAlertStorageXLS() new AlertStorage.xls created successfully..");
				}

			}catch(IOException ex)
			{
				cLogger.error(ex.getMessage());
				throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
						ex.toString());
			}catch(Exception e){
				cLogger.error("Exception in  TivoliAlertExcelUtil.createAlertStorageXLS():: "+e);
				throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTIL,
						e.toString());
			}
			
			return true;
		}
}
